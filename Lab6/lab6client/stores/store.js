
import { createStore } from 'vuex'

export const store = createStore({
	state: () => {
		return {
			client: {
				_id: 0,
				authorized: false,
				image: require('../assets/profile.svg'),
				name: 'unauthorized',
			},
			current: {
				list: 0,
				members: [
					//{
					//	_id: 0,
					//	image: require('../assets/profile.svg'),
					//	name: 'rr',
					//},
				],
				room: {
					//_id: 0,
					//members: [
					//	{
					//		_id: 0,
					//		image: require('../assets/profile.svg'),
					//		name: 'rr',
					//	},
					//],
					//messages: [
					//	{
					//		_id: 0,
					//		author: {
					//			_id: 0,
					//			image: require('../assets/profile.svg'),
					//			name: 'qq',
					//		},
					//		text: 'www',
					//	},
					//],
				},
				student: 0,
				task: 0,
			},
			notifications: [
				//{
				//	_id: 0,
				//	room: {
				//		_id: 0,
				//		image: require('../assets/profile.svg'),
				//		name: 'admin',
				//	},
				//	text: 'bruhhh',
				//},
			],
			rooms: [
				//{
				//	_id: 0,
				//	image: require('../assets/profile.svg'),
				//	name: 'fff',
				//},
			],
			show: {
				modal: {
					add: {
						member: false,
						student: false,
						task: false,
					},
					edit: {
						profile: false,
						student: false,
						task: false,
					},
					delete: {
						student: false,
                    },
					sign: {
						in: false,
						up: false,
					},
				},
				window: {
					notifications: false,
					profile: false,
				}
			},
			socket: {},
			students: [
				//{
				//	_id: 0,
				//	birthdate: '24.04.2004',
				//	firstName: 'Ann',
				//	gender: 'Female',
				//	group: 'KN',
				//	lastName: 'Bond',
				//	status: true,
				//},
			],
			taskList: {
				done: [
					//{ _id: 1, date: '2023-03-20', description: 'ff', name: 'Task #2' },
					//{ _id: 2, date: '2023-03-30', description: 'ff', name: 'Task #3' },
				],
				inProcces: [
					//{ _id: 3, date: '2023-03-10', description: 'ff', name: 'Task #1' },
				],
				toDo: [
					//{ _id: 4, date: '2023-03-01', description: 'ff', name: 'Task #0' },
				],
            },
		}
	}
})

//export const { state } = store