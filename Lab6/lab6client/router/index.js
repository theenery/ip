
import { createRouter, createWebHistory } from 'vue-router'
import { store } from '../stores/store'
let state = store.state

const routes = [
    {
        path: '/',
        name: 'home',
        component: () => import('../components/HomeModule.vue')
    },
    {
        path: '/messages',
        name: 'messages',
        component: () => import('../components/MessagesModule.vue')
    },
    {
        path: '/students',
        name: 'students',
        component: () => import('../components/StudentsModule.vue')
    },
    {
        path: '/tasks',
        name: 'tasks',
        component: () => import('../components/TasksModule.vue')
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

router.beforeEach((to, from) => {
    if (!state.client.authorized && to.name !== 'home') return { name: 'home' }
})

export default router
