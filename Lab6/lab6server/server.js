
const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const path = require('path');

const port = 3000;
const defaultProfileImageURL = 'https://www.svgrepo.com/show/497407/profile-circle.svg';

const mongoose = require('mongoose');
const uri = require('./uri');
mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("Connected successfully to server");
});

const Message = require('./models/message');
const Room = require('./models/room');
const Student = require('./models/student');
const Task = require('./models/task');
const User = require('./models/user');

async function addMember(_id, room_id) {
    try {
        let user = await User.findByIdAndUpdate(
            _id,
            { $push: { accessibleRooms: room_id } },
            { new: true, select: 'accessibleRooms image name socket' });
        let room = await Room.findByIdAndUpdate(
            room_id,
            { $push: { members: _id } },
            { select: 'image name' });
        if (user.socket) {
            io.sockets.sockets.get(user.socket).emit('newRoomAvailable', room);
            io.sockets.sockets.get(user.socket).join('room' + room_id);
        }
        {
            const { _id, image, name } = user;
            user = { _id, image, name };
        }
        io.sockets.in('room' + room_id).emit('newMemberConnected', user);
        console.log('User: ', user, '\nadded to room: ', room);
    }
    catch (err) {
        console.log('Error in addMember: ', err);
    }
}

async function addStudent(_student) {
    try {
        let student = await Student.create(_student);
        {
            const { _id, birthdate, firstName, gender, group, lastName, status } = student;
            student = { _id, birthdate, firstName, gender, group, lastName, status };
        }
        console.log('Student added: ', student);
        return student;
    }
    catch (err) {
        console.log('Error in addStudent: ', err);
    }
}

async function addTask(_task, _user, list) {
    try {
        let task = await Task.create(_task);
        {
            const { _id, date, description, name } = task;
            task = { _id, date, description, name };
        }
        let prop = ((index) => {
            let props = ['toDo', 'inProcces', 'done']
            return 'taskList.' + props[index]
        }) (list);
        let user = await User.findByIdAndUpdate(
            _user,
            { $push: { [prop]: task._id } },
            { new: true, select: 'name taskList' });
        console.log('Task: ', task, '\nadded to user: ', user);
        return task;
    }
    catch (err) {
        console.log('Error in addTask: ', err);
    }
}

async function changeStatus(_student) {
    try {
        let student = await Student.findByIdAndUpdate(
            _student._id,
            [{ $set: { status: { $eq: ['$status', false] } } }],
            { new: true, select: 'firstName lastName status' }
        );
        console.log('Students status changed: ', student);
    }
    catch (err) {
        console.log('Error in changeStatus: ', err);
    }
}

async function createRoom(creator, name) {
    try {
        let room = await Room.create({ members: [creator], name: name });
        let user = await User.findByIdAndUpdate(
            creator,
            { $push: { accessibleRooms: room._id } },
            { select: 'name' });
        {
            const { _id, image, name } = room;
            room = { _id, image, name };
        }
        console.log('User: ', user, '\ncreated room: ', room);
        return room;
    }
    catch (err) {
        console.log('Error in createRoom: ', err);
    }
}

async function deleteStudent(_student) {
    try {
        await Student.deleteOne({ _id: _student._id });
        console.log('Deleted student: ', _student);
    }
    catch (err) {
        console.log('Error in deleteStudent: ', err);
    }
}

async function editProfile(_id, image, name) {
    try {
        let user = await User.findByIdAndUpdate(
            _id,
            { $set: { image, name } },
            { new: true, select: 'image name' });
        console.log('User edited profile: ', user);
    }
    catch (err) {
        console.log('Error in editProfile: ', err);
    }
}

async function editStudent(_student) {
    try {
        let student = await Student.findByIdAndUpdate(
            _student._id,
            { $set: _student },
            { new: true, select: 'birthdate firstName gender group lastName status' });
        console.log('Student edited: ', student);
        return student;
    }
    catch (err) {
        console.log('Error in editStudent: ', err);
    }
}

async function editTask(_task, _user, list, oldList) {
    try {
        if (list !== oldList) {
            let getProp = (index) => {
                let props = ['toDo', 'inProcces', 'done']
                return 'taskList.' + props[index]
            }
            let user = await User.findByIdAndUpdate(
                _user,
                { $pull: { [getProp(oldList)]: _task._id }, $push: { [getProp(list)]: _task._id } },
                { new: true, select: 'name taskList' });
            console.log('Task edited in user: ', user);
        }
        let task = await Task.findByIdAndUpdate(
            _task._id,
            { $set: _task },
            { new: true, select: 'date description name' });
        console.log('Task edited: ', task);
        return task;
    }
    catch (err) {
        console.log('Error in editTask: ', err);
    }
}

async function fetchRoom(_id, requester) {
    try {
        let room = await Room.findOne({ _id: _id, members: requester }, 'members messages')
            .populate({ path: 'members', select: 'image name' });
        let user = await User.findByIdAndUpdate(
            requester,
            { $pull: { notifications: { $in: room.messages } } },
            { new: true, select: 'name notifications' })
            .populate({ path: 'notifications', select: 'room' });
        room = await room.populate({
                path: 'messages',
                select: '_id author text',
                populate:
                {
                    path: 'author',
                    select: 'image name'
                }
            });
        console.log('User: ', user, '\nfetched room: ', room);
        return room;
    }
    catch (err) {
        console.log('Error in fetchRoom: ', err);
    }
}

async function fetchStudents() {
    try {
        let students = await Student.find({}, '_id birthdate firstName gender group lastName status');
        console.log('Students fetched: ', students);
        return students;
    }
    catch (err) {
        console.log('Error in fetchStudents: ', err);
    }
}

async function logOut(_id, socket) {
    try {
        let user;
        if (_id) {
            user = await User.findByIdAndUpdate(
                _id,
                { $unset: { socket: null } },
                { select: 'accessibleRooms name' });
        }
        else {
            user = await User.findOneAndUpdate(
                { socket },
                { $unset: { socket: null } },
                { select: 'accessibleRooms name' });
            if (!user) return; // The client may not be authorized
        }
        user.accessibleRooms.forEach((room) => {
            io.sockets.sockets.get(socket).leave('room' + room._id.toString());
        });
        console.log('User logged out: ', user);
    }
    catch (err) {
        console.log('Error in logOut: ', err);
    }
}

async function saveNotification(message, user_id) {
    try {
        let user = await User.findByIdAndUpdate(
            user_id,
            { $push: { notifications: message } },
            { new: true, select: { name: 1, notifications: { $slice: -1 } } });
        console.log('Notification saved for user: ', user);
    }
    catch (err) {
        console.log('Error in saveNotification: ', err);
    }
}

async function searchMembers(name, requester, room_id) {
    try {
        let room = await Room.findById(room_id, 'members');
        let users = await User.find(
            { name: { $regex: name, $options: 'i' }, _id: { $ne: requester, $nin: room.members } },
            'image name');
        console.log('Searched users: ', users);
        return users;
    }
    catch (err) {
        console.log('Error in searchMembers: ', err);
    }
}

async function sendMessage(author, room_id, text) {
    try {
        let message = await Message.create({ author: author, room: room_id, text: text });
        let room = await Room.findOneAndUpdate(
            { _id: room_id, members: author },
            { $push: { messages: { _id: message._id } } },
            { new: true, select: 'members' })
            .populate({ path: 'members', select: 'socket' });
        room.members.forEach((member) => {
            if (!member.socket) {
                saveNotification(message._id, member._id);
            }
        });
        message = await Message.findById(
            message._id, '_id author room text')
            .populate({ path: 'author', select: 'image name' })
            .populate({ path: 'room', select: 'image name' });
        console.log('New message: ', message);
        return message;
    }
    catch (err) {
        console.log('Error in sendMessage: ', err);
    }
}

async function signIn(login, password, socket) {
    try {
        let user = await User.findOneAndUpdate(
            { login, password },
            { $set: { socket } },
            { select: 'accessibleRooms image name notifications taskList' })
            .populate({ path: 'accessibleRooms', select: 'image name' })
            .populate({ path: 'notifications', select: '_id room text', populate: { path: 'room', select: 'image name' } })
            .populate({ path: 'taskList.done taskList.inProcces taskList.toDo', select: 'date description name' });
        if (!user) {
            console.log('The client may enter wrong login or password, entered login: ', login);
            return;
        }
        user.accessibleRooms.forEach((room) => {
            io.sockets.sockets.get(socket).join('room' + room._id.toString());
        });
        console.log('User signed in: ', user);
        return user;
    }
    catch (err) {
        console.log('Error in signIn: ', err);
    }
}

async function signUp(login, name, password, socket) {
    try {
        let user = await User.create({ login, name, password, socket });
        {
            const { _id, accessibleRooms, image, name, notifications, taskList } = user;
            user = { _id, accessibleRooms, image, name, notifications, taskList };
        }
        console.log('User created: ', user);
        return user;
    }
    catch (err) {
        if (err.code === 11000) {
            console.log('The user tried to create a profile with an existing login: ', err.keyValue);
            return { loginDuplicated: true };
        }
        console.log('Error in signUp: ', err);
    }
}

//-------------------------------------------------------------

io.on('connection', (socket) => {
    socket.on('addMember', (_id, room_id) => {
        addMember(_id, room_id);
    });

    socket.on('addStudent', async (_student) => {
        let student = await addStudent(_student);
        socket.emit('addStudentResponse', student);
    });

    socket.on('addTask', async (_task, _user, list) => {
        let task = await addTask(_task, _user, list);
        socket.emit('newTaskAvailable', list, task);
    });

    socket.on('changeStatus', (student) => {
        changeStatus(student);
    });

    socket.on('createRoom', async (creator, name) => {
        let room = await createRoom(creator, name);
        socket.join('room' + room._id.toString());
        socket.emit('newRoomAvailable', room);
    });

    socket.on('deleteStudent', (student) => {
        deleteStudent(student);
    });

    socket.on('disconnect', () => {
        logOut(null, socket.id);
    });

    socket.on('editProfile', (_id, image, name) => {
        editProfile(_id, image, name);
    });

    socket.on('editStudent', async (_student) => {
        let student = await editStudent(_student);
        socket.emit('editStudentResponse', student);
    });

    socket.on('editTask', async (_task, _user, list, oldList) => {
        let task = await editTask(_task, _user, list, oldList);
        socket.emit('editTaskResponse', task, list, oldList);
    });

    socket.on('fetchRoom', async (_id, requester) => {
        let room = await fetchRoom(_id, requester);
        socket.emit('fetchRoomResponse', room);
    });

    socket.on('fetchStudents', async () => {
        let students = await fetchStudents();
        socket.emit('fetchStudentsResponse', students);
    });

    socket.on('logOut', (_id) => {
        logOut(_id, socket.id);
    });

    socket.on('saveNotification', (message, user_id) => {
        saveNotification(message, user_id);
    });

    socket.on('searchMembers', async (name, requester, room_id) => {
        let members = await searchMembers(name, requester, room_id);
        socket.emit('searchMembersResponse', members);
    });

    socket.on('sendMessage', async (author, room_id, text) => {
        let message = await sendMessage(author, room_id, text);
        io.sockets.in('room' + message.room._id.toString()).emit('newMessageReceived', message);
    });

    socket.on('signIn', async (login, password) => {
        let user = await signIn(login, password, socket.id);
        socket.emit('signInResponse', user);
    });

    socket.on('signUp', async (login, name, password) => {
        let user = await signUp(login, name, password, socket.id);
        socket.emit('signInResponse', user);
    });

});

app.use(express.static('../lab6client/dist'));

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '../lab6client/dist/index.html'));
});

app.get('/messages/', (req, res) => {
    res.sendFile(path.join(__dirname, '../lab6client/dist/index.html'));
});

app.get('/students/', (req, res) => {
    res.sendFile(path.join(__dirname, '../lab6client/dist/index.html'));
});

app.get('/tasks/', (req, res) => {
    res.sendFile(path.join(__dirname, '../lab6client/dist/index.html'));
});

http.listen(port);