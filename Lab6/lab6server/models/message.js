
const mongoose = require('mongoose');
const objectId = mongoose.Types.ObjectId;

const messageSchema = new mongoose.Schema({
    author: { type: objectId, required: true, ref: 'User' },
    room: { type: objectId, required: true, ref: 'Room' },
    text: { type: String, required: true, maxLength: 1024 },
});

module.exports = mongoose.model('Message', messageSchema);