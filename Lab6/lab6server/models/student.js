
const mongoose = require('mongoose');

const studentSchema = new mongoose.Schema({
    birthdate: { type: String, required: true, maxLength: 10 },
    firstName: { type: String, required: true, maxLength: 32 },
    gender: { type: String, required: true, maxLength: 16 },
    group: { type: String, required: true, maxLength: 16 },
    lastName: { type: String, required: true, maxLength: 32 },
    status: { type: Boolean, default: false },
});

module.exports = mongoose.model('Student', studentSchema);