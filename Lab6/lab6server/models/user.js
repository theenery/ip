
const mongoose = require('mongoose');
const defaultImageURL = 'https://www.svgrepo.com/show/497407/profile-circle.svg';
const objectId = mongoose.Types.ObjectId;

const userSchema = new mongoose.Schema({
    accessibleRooms: [{ type: objectId, ref: 'Room' }],
    image: { type: String, default: defaultImageURL },
    login: { type: String, required: true, unique: true, maxLength: 32 },
    name: { type: String, required: true, maxLength: 32 },
    notifications: [{ type: objectId, ref: 'Message' }],
    password: { type: String, required: true, maxLength: 32 },
    socket: { type: String, default: null },
    taskList: {
        done: [{ type: objectId, ref: 'Task' }],
        inProcces: [{ type: objectId, ref: 'Task' }],
        toDo: [{ type: objectId, ref: 'Task' }],
    },
});

module.exports = mongoose.model('User', userSchema);