
const mongoose = require('mongoose');
const defaultImageURL = 'https://www.svgrepo.com/show/497407/profile-circle.svg';
const objectId = mongoose.Types.ObjectId;

const roomSchema = new mongoose.Schema({
    image: { type: String, default: defaultImageURL },
    members: [{ type: objectId, ref: 'User' }],
    messages: [{ type: objectId, ref: 'Message' }],
    name: { type: String, required: true, maxLength: 32 },
});

module.exports = mongoose.model('Room', roomSchema);