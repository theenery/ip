
const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
    date: { type: String, required: true, maxLength: 10 },
    description: { type: String, default: '', maxLength: 128 },
    name: { type: String, required: true, maxLength: 32 },
});

module.exports = mongoose.model('Task', taskSchema);