<?php

class Student {
	private $status;
	private $group;
	private $firstName;
	private $lastName;
	private $gender;
	private $birthdate;
	private $rowId;

	public function __construct($status, $group, $firstName, $lastName, $gender, $birthdate, $rowId) {
	$this->status = $status;
    $this->group = $group;
    $this->firstName = $firstName;
    $this->lastName = $lastName;
    $this->gender = $gender;
    $this->birthdate = $birthdate;
    $this->rowId = $rowId;
	}

	public function getStatus() {
		return $this->status;
	}

	public function setStatus($status) {
		$this->status = $status;
	}

	public function getGroup() {
		return $this->group;
	}

	public function setGroup($group) {
		$this->group = $group;
	}

	public function getFirstName() {
		return $this->firstName;
	}

	public function setFirstName($firstName) {
		$this->firstName = $firstName;
	}

	public function getLastName() {
		return $this->lastName;
	}

	public function setLastName($lastName) {
		$this->lastName = $lastName;
	}
	
	public function getGender() {
		return $this->gender;
	}

	public function setGender($gender) {
		$this->gender = $gender;
	}
	
	public function getBirthdate() {
		return $this->birthdate;
	}

	public function setBirthdate($birthdate) {
		$this->birthdate = $birthdate;
	}
	
	public function getRowId() {
		return $this->rowId;
	}

	public function setRowId($rowId) {
		$this->rowId = $rowId;
	}
}

class StudentsDAO {
	private $pdo;

	public function __construct($pdo) {
		$this->pdo = $pdo;
	}

	public function fetchStudents() {
		try {
			$stmt = $this->pdo->prepare("SELECT * FROM Students");
			$stmt->execute();
			return $stmt->fetchAll(PDO::FETCH_ASSOC);
		}
		catch(PDOException $e) {
			send_error("Cannot fetch data: " . $e->getMessage());
		}
	}

	public function addStudent($student) {
		try {
			$stmt = $this->pdo->prepare("INSERT INTO Students (Status, GroupName, FirstName, LastName, Gender, Birthdate) VALUES (?, ?, ?, ?, ?, ?)");
			$stmt->execute([$student->getStatus(),
							$student->getGroup(),
							$student->getFirstName(),
							$student->getLastName(),
							$student->getGender(),
							$student->getBirthdate()]);
			$stmt = $this->pdo->prepare("SELECT LAST_INSERT_ID()");
			$stmt->execute();
			return $stmt->fetch(PDO::FETCH_ASSOC);
		}
		catch(PDOException $e) {
			send_error("Cannot add student: " . $e->getMessage());
		}
	}

	public function editStudent($student) {
		try {
			$stmt = $this->pdo->prepare("UPDATE Students SET GroupName=?, FirstName=?, LastName=?, Gender=?, Birthdate=? WHERE Id=?");
			$stmt->execute([$student->getGroup(),
							$student->getFirstName(),
							$student->getLastName(),
							$student->getGender(),
							$student->getBirthdate(),
							$student->getRowId()]);
		}
		catch(PDOException $e) {
			send_error("Cannot edit student: " . $e->getMessage());
		}
	}

	public function changeStudentsStatus($status, $rowId) {
		try {
			$stmt = $this->pdo->prepare("UPDATE Students SET Status=? WHERE Id=?");
			$stmt->execute([$status, $rowId]);
		}
		catch(PDOException $e) {
			send_error("Cannot change status: " . $e->getMessage());
		}
	}

	public function deleteStudent($rowId) {
		try {
			$stmt = $this->pdo->prepare("DELETE FROM Students WHERE Id=?");
			$stmt->execute([$rowId]);
		}
		catch(PDOException $e) {
			send_error("Cannot delete student: " . $e->getMessage());
		}
	}

	public function canSignIn($login, $password) {
		try {
			$stmt = $this->pdo->prepare("SELECT Password FROM SignIn WHERE Login=?");
			$stmt->execute([$login]);
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			if(empty($result)) return false;
			$obtainedPassword = $result["Password"];
			return $obtainedPassword == $password;
		}
		catch(PDOException $e) {
			send_unsuccessful_sign_in();
		}
	}

}

?>