<?php

require_once 'students-model.php';
require_once 'students-view.php';

// Clear our input data
function test_input($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

$studentsDAO;
try {
	$conn = new PDO("mysql:host=localhost;dbname=lab4server", "Lab4Server", "ReDcIm50()oXCmC8");
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$studentsDAO = new StudentsDAO($conn);
}
catch (PDOException $e) {
	send_error("DB Connection failed: " . $e->getMessage());
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if($_POST["action"] == "signIn") {
		$login = test_input($_POST["login"]);
		$password = test_input($_POST["password"]);
		if($studentsDAO->canSignIn($login, $password)) {
			send_successful_sign_in();
		}
		else {
			send_unsuccessful_sign_in();
		}
	}
	else if($_POST["action"] == "fetch") {
		$data = $studentsDAO->fetchStudents();
		send_table_data($data);
	}
	else if($_POST["action"] == "changeStatus") {
		$status = filter_var(test_input($_POST["status"]), FILTER_VALIDATE_BOOLEAN);
		$rowId = test_input($_POST["row-id"]);
		$studentsDAO->changeStudentsStatus($status, $rowId);
		send_edited_status($status, $rowId);
	}
	else if($_POST["action"] == "delete") {
		$rowId = test_input($_POST["row-id"]);
		$studentsDAO->deleteStudent($rowId);
		send_successful_delete($rowId);
	}
	else if($_POST["action"] == "add" || $_POST["action"] == "edit") {
		// Get and clear data
		$student = new Student(	test_input($_POST["status"]),
								test_input($_POST["group"]),
								test_input($_POST["first-name"]),
								test_input($_POST["last-name"]),
								test_input($_POST["gender"]),
								test_input($_POST["birthdate"]),
								test_input($_POST["row-id"])
		);

		// Checking if empty
		if(empty($student->getGroup())) {
			send_error("Choose the group.");
		} else if (empty($student->getFirstName())) {
			send_error("Input the first name.");
		} else if (empty($student->getLastName())) {
			send_error("Input the last name.");
		} else if(empty($student->getGender())){
			send_error("Choose the gender.");
		} else if (empty($student->getBirthdate())) {
			send_error("Input the birthdate");
		}

		// Some data for validation
		$groups = array("KN-21", "KN-22");
		$genders = array("Male", "Female");
		$minBirthDate = "1950-01-01";
		$maxBirthDate = "2010-01-01";
		// Validation
		if(!in_array($student->getGroup(), $groups)) {
			send_error("Invalid group.");
		} else if (!preg_match("/^[a-zA-Z-']*$/", $student->getFirstName())) {
			send_error("First name contains invalid characters.");
		} else if (!preg_match("/^[a-zA-Z-']*$/", $student->getLastName())) {
			send_error("Last name contains invalid characters.");
		} else if(!in_array($student->getGender(), $genders)){
			send_error("Invalid gender.");
		} else if ($student->getBirthdate() < $minBirthDate) {
			send_error("You are too old :c");
		} else if ($student->getBirthdate() > $maxBirthDate) {
			send_error("You are too young :c");
		}

		if($_POST["action"] == "add") {
			$rowId = $studentsDAO->addStudent($student)["LAST_INSERT_ID()"];
			$student->setRowId($rowId);
		}
		else if($_POST["action"] == "edit") {
			$studentsDAO->editStudent($student);
		}

		// Success - sending the data back
		send_student_data($student);
	}
}

?>