<?php

// Quick stop execution
function send_error($message) {
	echo json_encode(array("isInvalid" => true, "errorMessage" => $message));
	exit;
}

// Sending the student data back
function send_student_data($student) {
	echo json_encode(array(	"group" => $student->getGroup(),
							"firstName" => $student->getFirstName(),
							"lastName" => $student->getLastName(),
							"gender" => $student->getGender(),
							"birthdate" => $student->getBirthdate(),
							"rowId" => $student->getRowId(),
							"isInvalid" => false));
}

// Sending the table data
function send_table_data($tableData) {
	$students = array();
	if(empty($tableData)) send_error("Table is empty now");
	foreach($tableData as $row) {
		$students[] = array(	"status" => $row["Status"],
								"group" => $row["GroupName"],
								"firstName" => $row["FirstName"],
								"lastName" => $row["LastName"],
								"gender" => $row["Gender"],
								"birthdate" => $row["Birthdate"],
								"rowId" => $row["Id"]);
	}
	echo json_encode(array(	"array" => $students,
							"isInvalid" => false));
}

// Sending the changed status
function send_edited_status($status, $rowId) {
	echo json_encode(array(	"isInvalid" => false,
							"status" => $status,
							"rowId" => $rowId));
}

// Sending check if wrong row deleted
function send_successful_delete($rowId) {
	echo json_encode(array(	"isInvalid" => false,
							"rowId" => $rowId));
}

function send_successful_sign_in() {
	echo json_encode(array(	"isInvalid" => false));
}

function send_unsuccessful_sign_in() {
	echo json_encode(array(	"isInvalid" => true));
}

?>