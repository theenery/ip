
function notification() {
    const anim = [
        { opacity: 0 },
        { opacity: 1 },
    ];

    const params = {
        duration: 1000,
        delay: 1000,
        iterations: 5,
        direction: "alternate",
        fill: "forwards",
    };

    document.getElementById("notification-circle").animate(anim, params);
}

function openNotifications() {
    document.getElementById("notification-window").style.display = "block";
}

function closeNotifications() {
    document.getElementById("notification-window").style.display = "none";
}

function openProfile() {
    document.getElementById("profile-window").style.display = "block";
}

function closeProfile() {
    document.getElementById("profile-window").style.display = "none";
}

function openSignInModal() {
    $("#sign-in-modal").show();
}

function closeSignInModal() {
    $("#sign-in-modal").hide();
}

function signIn() {
    let login = $("form[name=sign-in-form] [name=login]").val();
    let password = $("form[name=sign-in-form] [name=password]").val();
    // I don`t care about it due to "required" in html
    if (login == "" || password == "") return false;
    let string = $("form[name=sign-in-form]").serialize();
    $.post("students-php.php", string + "&action=signIn", function (data, status) {
        data = jQuery.parseJSON(data);
        if (data.isInvalid == false) {
            $(".profile-logged").show();
            $(".profile-unlogged").hide();
            closeSignInModal();
        } else {
            alert("Wrong login or password");
        }
    });
    event.preventDefault();
}

function openAddStudentModal() {
    $("form[name=add-edt-form]").trigger("reset");
    $("#add-edt-modal-heading").html("Add student");
    $("#add-edt-modal-ok-btn").attr("onclick", "addStudent()"); //
    $("#add-edt-modal").show();
}

let rowToEdit;

function openEditStudentModal(event) {
    rowToEdit = event.target.parentNode.parentNode.parentNode.parentNode;
    let data = getRowData(rowToEdit);
    setAddEdtFormData(data);
    $("#add-edt-modal-heading").html("Edit student");
    $("#add-edt-modal-ok-btn").attr("onclick", "editStudent()"); //
    $("#add-edt-modal").show();
}

function closeAddEditStudentModal() {
    $("#add-edt-modal").hide();
}

class RowData {
    constructor(status, group, firstName, lastName, gender, birthdate, rowId) {
        this.status = status;
        this.group = group;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.birthdate = birthdate;
        this.rowId = rowId;
    }
}

function serializeAddEdtForm() {
    let string = $("form[name=add-edt-form]").serialize();
    //alert(string);
    return string;
}

function getAddEdtFormData() {
    let status = $("form[name=add-edt-form] [name=status]").val();
    let group = $("form[name=add-edt-form] [name=group]").val();
    let firstName = $("form[name=add-edt-form] [name=first-name]").val();
    let lastName = $("form[name=add-edt-form] [name=last-name]").val();
    let gender = $("form[name=add-edt-form] [name=gender]").val();
    let bdStr = $("form[name=add-edt-form] [name=birthdate]").val();
    let birthdate = new Date(bdStr);
    let rowId = $("form[name=add-edt-form] [name=row-id]").val();
    return new RowData(status, group, firstName, lastName, gender, birthdate, rowId);
}

function setAddEdtFormData(data) {
    $("form[name=add-edt-form] [name=status]").val(data.status);
    $("form[name=add-edt-form] [name=group]").val(data.group);
    $("form[name=add-edt-form] [name=first-name]").val(data.firstName);
    $("form[name=add-edt-form] [name=last-name]").val(data.lastName);
    $("form[name=add-edt-form] [name=gender]").val(data.gender);
    let bdStr = data.birthdate.getFullYear() + "-" + String(data.birthdate.getMonth() + 1).padStart(2, "0") + "-" + String(data.birthdate.getDate()).padStart(2, "0");
    $("form[name=add-edt-form] [name=birthdate]").val(bdStr);
    $("form[name=add-edt-form] [name=row-id]").val(data.rowId);
}

function isValidAddEdtForm() {
    // Data for validation
    let data = getAddEdtFormData();
    let letters = /^[A-Za-z]+$/;
    let maxDate = new Date(2023, 1, 1);
    let minDate = new Date(1923, 1, 1);

    // I don`t care about it due to "required" in html
    if (data.firstName == "" || data.lastName == "" || data.birthdate.toDateString() == "Invalid Date") {
        return false;
    }

    // Light validation
    if (!data.firstName.match(letters)) {
        alert("Uncorrect first name");
        $("form[name=add-edt-form] [name=first-name]").select();
        event.preventDefault();
        return false;
    } else if (!data.lastName.match(letters)) {
        alert("Uncorrect last name");
        $("form[name=add-edt-form] [name=last-name]").select();
        event.preventDefault();
        return false;
    } else if (data.birthdate > maxDate || data.birthdate < minDate) {
        alert("Uncorrect birthdate");
        $("form[name=add-edt-form] [name=birthdate]").select();
        event.preventDefault();
        return false;
    }
    return true;
}

function createRow() {
    $("#main-table tbody").append(
        '<tr>\
            <td>\
                <input type="checkbox" onclick="changeState(event)" />\
            </td>\
            <td></td>\
            <td></td>\
            <td></td>\
            <td></td>\
            <td>\
                <div class="status"></div>\
            </td>\
            <td>\
                <button onclick="openEditStudentModal(event)">\
                    <div class="btn-icon">\
                        <img src="./img/edit.svg" alt="E" height="28px" />\
                    </div>\
                </button>\
                <button onclick="openWarningModal(event)">\
                    <div class="btn-icon">\
                        <img src="./img/delete.svg" alt="D" height="28px" />\
                    </div>\
                </button>\
            </td>\
        </tr>'
    );
}

function fillRow(row, data) {
    if (data.status) {
        $(row).children().eq(0).children().eq(0)[0].checked = data.status;
        $(row).children().eq(5).children().eq(0)[0].style.backgroundColor = "greenyellow";
    }
    $(row).children().eq(1).html(data.group);
    $(row).children().eq(2).html(data.firstName + " " + data.lastName);
    $(row).children().eq(3).html(data.gender[0]);
    $(row).children().eq(4).html(String(data.birthdate.getDate()).padStart(2, "0") + "." + String(data.birthdate.getMonth() + 1).padStart(2, "0") + "." + data.birthdate.getFullYear());
    $(row).data("id", data.rowId);
}

function getRowData(row) {
    let status = $(row).children().eq(0).children().eq(0).prop("checked");
    let group = $(row).children().eq(1).text();
    let firstName = $(row).children().eq(2).text().split(" ")[0];
    let lastName = $(row).children().eq(2).text().split(" ")[1];
    let gender = $(row).children().eq(3).text() == "M" ? "Male" : "Female";
    let bdStr = $(row).children().eq(4).text().split(".");
    let birthdate = new Date(parseInt(bdStr[2], 10),
                             parseInt(bdStr[1], 10) - 1,
                             parseInt(bdStr[0], 10));
    let rowId = $(row).data("id");
    return new RowData(status, group, firstName, lastName, gender, birthdate, rowId);
}

function sendValidateAndUpdateData(serializedData, action) {
    $.post("students-php.php", serializedData + "&action=" + action, function (data, status) {
        //alert("Data: " + data + "\nStatus: " + status);
        data = jQuery.parseJSON(data);
        if (data.isInvalid === true) {
            alert(data.errorMessage);
        } else {
            validRowData = new RowData(
                data.status,
                data.group,
                data.firstName,
                data.lastName,
                data.gender,
                new Date(data.birthdate),
                data.rowId);
            if (action === "add") {
                createRow();
                rowToEdit = document.getElementById("main-table").children[0].lastChild;
            }
            fillRow(rowToEdit, validRowData);
            closeAddEditStudentModal();
        }
    });
}

function addStudent() {
    if (isValidAddEdtForm() === false) return false;
    let stringToSend = serializeAddEdtForm();
    sendValidateAndUpdateData(stringToSend, "add");
    // I don`t want to refresh the page
    event.preventDefault();
}

function editStudent() {
    if (isValidAddEdtForm() === false) return false;
    let stringToSend = serializeAddEdtForm();
    sendValidateAndUpdateData(stringToSend, "edit");
    // I don`t want to refresh the page
    event.preventDefault();
}

function changeState(event) {
    let checkbox = event.target;
    let row = checkbox.parentNode.parentNode;
    let rowId = $(row).data("id");
    let dataString = "action=changeStatus" + "&status=" + checkbox.checked + "&row-id=" + rowId;
    $.post("students-php.php", dataString, function (data, status) {
        //alert("Data: " + data + "\nStatus: " + status);
        data = jQuery.parseJSON(data);
        if (data.isInvalid === true) {
            alert(data.errorMessage);
        } else {
            if (data.rowId != rowId) {
                alert("The status of the wrong row was changed...\nNeed to change " + rowId + " but " + data.rowId + " returned");
            }
            let statusDiv = row.children[5].children[0];
            if (data.status) statusDiv.style.backgroundColor = "greenyellow";
            else statusDiv.style.backgroundColor = "lightgrey";
        }
    });
}

function fetchAndUpdate() {
    $.post("students-php.php", "action=fetch", function (data, status) {
        //alert("Data: " + data + "\nStatus: " + status);
        data = jQuery.parseJSON(data);
        if (data.isInvalid === true) {
            alert(data.errorMessage);
        } else {
            data.array.forEach(function (element) {
                createRow();
                rowToEdit = document.getElementById("main-table").children[0].lastChild;
                validRowData = new RowData(
                    element.status,
                    element.group,
                    element.firstName,
                    element.lastName,
                    element.gender,
                    new Date(element.birthdate),
                    element.rowId);
                fillRow(rowToEdit, validRowData);
            });
        }
    });
}
fetchAndUpdate();

function openWarningModal(event) {
    let img = event.target;
    rowToEdit = img.parentNode.parentNode.parentNode.parentNode;
    let name = rowToEdit.children[2].innerText;
    document.getElementById("del-name").innerText = name;
    document.getElementById("del-modal").style.display = "block";
}

function closeWarningModal() {
    document.getElementById("del-modal").style.display = "none";
}

function deleteRow() {
    let rowId = $(rowToEdit).data("id");
    $.post("students-php.php", "action=delete&row-id=" + rowId, function (data, status) {
        //alert("Data: " + data + "\nStatus: " + status);
        data = jQuery.parseJSON(data);
        if (data.isInvalid === true) {
            alert(data.errorMessage);
        } else {
            if (data.rowId != rowId) {
                alert("The wrong row was deleted...\nNeed to delete " + rowId + " but " + data.rowId + " removed")
            }
            rowToEdit.remove();
        }
    });
    closeWarningModal();
}