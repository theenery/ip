<?php

require_once 'students-model.php';
require_once 'students-view.php';

// Clear our input data
function test_input($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if($_POST["action"] == "signIn") {
		$login = test_input($_POST["login"]);
		$password = test_input($_POST["password"]);
		if($login == "hehe" && $password == "nothehe") {
			echo json_encode(array("success" => true));
		}
		else {
			echo json_encode(array("success" => false));
		}
	}
	else {
		// Get and clear data
		$student = new Student(	test_input($_POST["group"]),
								test_input($_POST["first-name"]),
								test_input($_POST["last-name"]),
								test_input($_POST["gender"]),
								test_input($_POST["birthdate"]),
								test_input($_POST["row-id"])
		);

		// Checking if empty
		if(empty($student->getGroup())) {
			send_error("Choose the group.");
		} else if (empty($student->getFirstName())) {
			send_error("Input the first name.");
		} else if (empty($student->getLastName())) {
			send_error("Input the last name.");
		} else if(empty($student->getGender())){
			send_error("Choose the gender.");
		} else if (empty($student->getBirthdate())) {
			send_error("Input the birthdate");
		}

		// Some data for validation
		$groups = array("KN-21", "KN-22");
		$genders = array("Male", "Female");
		$minBirthDate = "1950-01-01";
		$maxBirthDate = "2010-01-01";
		// Validation
		if(!in_array($student->getGroup(), $groups)) {
			send_error("Invalid group.");
		} else if (!preg_match("/^[a-zA-Z-']*$/", $student->getFirstName())) {
			send_error("First name contains invalid characters.");
		} else if (!preg_match("/^[a-zA-Z-']*$/", $student->getLastName())) {
			send_error("Last name contains invalid characters.");
		} else if(!in_array($student->getGender(), $genders)){
			send_error("Invalid gender.");
		} else if ($student->getBirthdate() < $minBirthDate) {
			send_error("You are too old :c");
		} else if ($student->getBirthdate() > $maxBirthDate) {
			send_error("You are too young :c");
		}

		// Success - sending the data back
		send_data($student);
	}
}

?>