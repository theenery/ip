<?php

class Student {
	private $group;
	private $firstName;
	private $lastName;
	private $gender;
	private $birthdate;
	private $rowId;

	public function __construct($group, $firstName, $lastName, $gender, $birthdate, $rowId) {
    $this->group = $group;
    $this->firstName = $firstName;
    $this->lastName = $lastName;
    $this->gender = $gender;
    $this->birthdate = $birthdate;
    $this->rowId = $rowId;
	}

	public function getGroup() {
		return $this->group;
	}

	public function setGroup($group) {
		$this->group = $group;
	}

	public function getFirstName() {
		return $this->firstName;
	}

	public function setFirstName($firstName) {
		$this->firstName = $firstName;
	}

	public function getLastName() {
		return $this->lastName;
	}

	public function setLastName($lastName) {
		$this->lastName = $lastName;
	}
	
	public function getGender() {
		return $this->gender;
	}

	public function setGender($gender) {
		$this->gender = $gender;
	}
	
	public function getBirthdate() {
		return $this->birthdate;
	}

	public function setBirthdate($birthdate) {
		$this->birthdate = $birthdate;
	}
	
	public function getRowId() {
		return $this->rowId;
	}

	public function setRowId($rowId) {
		$this->rowId = $rowId;
	}
}

?>