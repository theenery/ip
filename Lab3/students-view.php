<?php

// Quick stop execution
function send_error($message) {
	echo json_encode(array("isInvalid" => true, "errorMessage" => $message));
	exit;
}

// Sending the data back
function send_data($student) {
	echo json_encode(array(	"group" => $student->getGroup(),
							"firstName" => $student->getFirstName(),
							"lastName" => $student->getLastName(),
							"gender" => $student->getGender(),
							"birthdate" => $student->getBirthdate(),
							"rowId" => $student->getRowId(),
							"isInvalid" => false));
}

?>