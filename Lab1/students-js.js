
function notification() {
    const anim = [
        { opacity: 0 },
        { opacity: 1 },
    ];

    const params = {
        duration: 1000,
        delay: 1000,
        iterations: 5,
        direction: "alternate",
        fill: "forwards",
    };

    document.getElementById("notification-circle").animate(anim, params);
}

function openNotifications() {
    document.getElementById("notification-window").style.display = "block";
}

function closeNotifications() {
    document.getElementById("notification-window").style.display = "none";
}

function openProfile() {
    document.getElementById("profile-window").style.display = "block";
}

function closeProfile() {
    document.getElementById("profile-window").style.display = "none";
}

function openAddStudentModal() {
    document.getElementById("add-modal").style.display = "block";
}

function closeAddStudentModal() {
    document.getElementById("add-modal").style.display = "none";
}

function addStudent() {
    let tr = document.createElement("tr");

    let td_checkbox = document.createElement("td");
    let checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.setAttribute("onclick", "changeState(event)");
    td_checkbox.append(checkbox);

    let td_group = document.createElement("td");
    td_group.append("PZ-22");

    let td_name = document.createElement("td");
    td_name.append("Oleksandr Kozyra");

    let td_gender = document.createElement("td");
    td_gender.append("M");

    let td_birthday = document.createElement("td");
    td_birthday.append("16.07.2004");

    let td_status = document.createElement("td");
    let status = document.createElement("div");
    status.classList.add("status");
    td_status.append(status);

    let td_options = document.createElement("td");

    let ed_button = document.createElement("button");
    let ed_div = document.createElement("div");
    ed_div.classList.add("btn-icon");
    let ed_img = document.createElement("img");
    ed_img.src = "./img/edit.svg";
    ed_img.alt = "E";
    ed_img.height = 28;
    ed_div.append(ed_img);
    ed_button.append(ed_div);

    let del_button = document.createElement("button");
    del_button.setAttribute("onclick", "openWarningModal(event)");
    let del_div = document.createElement("div");
    del_div.classList.add("btn-icon");
    let del_img = document.createElement("img");
    del_img.src = "./img/delete.svg";
    del_img.alt = "E";
    del_img.height = 28;
    del_div.append(del_img);

    del_button.append(del_div);
    td_options.append(ed_button, "\n", del_button);
    
    tr.append(td_checkbox, td_group, td_name, td_gender, td_birthday, td_status, td_options);
    document.getElementById("main-table").children[0].append(tr);

    closeAddStudentModal();
}

function changeState(event) {
    let checkbox = event.target;
    let row = checkbox.parentNode.parentNode;
    let statusDiv = row.children[5].children[0];
    if (checkbox.checked) statusDiv.style.backgroundColor = "greenyellow";
    else statusDiv.style.backgroundColor = "lightgrey";
}

let rowToRemove;

function openWarningModal(event) {
    let img = event.target;
    rowToRemove = img.parentNode.parentNode.parentNode.parentNode;
    let name = rowToRemove.children[2].innerText;
    document.getElementById("del-name").innerText = name;
    document.getElementById("del-modal").style.display = "block";
}

function closeWarningModal() {
    document.getElementById("del-modal").style.display = "none";
}

function deleteRow() {
    rowToRemove.remove();
    closeWarningModal();
}