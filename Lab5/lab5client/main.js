
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
//import { store } from './stores/store'
import './reset.css'

const app = createApp(App).use(router)
app.use(router)
//app.use(store)
app.mount('#app')
