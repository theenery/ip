
import { Socket } from 'socket.io-client'
import { createRouter, createWebHistory } from 'vue-router'
import { store } from '../stores/store'

const routes = [
    {
        path: '/',
        name: 'home',
        component: () => import('../components/HomeModule.vue')
    },
    {
        path: '/messages',
        name: 'messages',
        component: () => import('../components/MessagesModule.vue')
    },
    {
        path: '/students',
        name: 'students',
        component: () => import('../components/StudentsModule.vue')
    },
    {
        path: '/tasks',
        name: 'tasks',
        component: () => import('../components/TasksModule.vue')
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

router.beforeEach((to, from) => {
    let socket = store.state.socket
    let signIn = store.state.signIn
    let currentRoom = store.state.currentRoom
    let notifications = store.state.notifications
    if (from.name === 'messages') socket.emit('leave', currentRoom.id)
    if (to.name === 'messages') {
        notifications.length = 0
        if (store.state.client.id) store.state.socket.emit('notificationsReaded', store.state.client.id)
        document.getElementById("notification-circle").getAnimations().forEach((anim) => anim.cancel())
    }
    if (signIn.state === false && to.name !== 'home') return { name: 'home' }
})

export default router
