
import { createStore } from 'vuex'

export const store = createStore({
	state: () => {
		return {
			socket: {},
			client: {
				id: 0,
				name: 'unauthorized',
				image: require('../assets/profile.svg'),
				profileModalVisibility: false,
				addMemberModalVisibility: false,
				addTaskModal: false,
				editTaskModal: false,
				taskToEdit: 0,
				listToEdit: 0
			},
			signIn: {
				state: false,
				modalVisibility: false,
				upModal: false
			},
			currentRoom: {},
			rooms: [],
			show: {
				profileWindow: false,
				notificationWindow: false
			},
			notifications: [
				//{
				//	room: {
				//		name: 'admin',
				//		image: require('../assets/profile.svg')
				//	},
				//	text: 'bruhhh',
				//	id: 1
				//}
			],
			students: [
				//{
				//	group: 'KN',
				//	firstName: 'Ann',
				//	lastName: 'Bond',
				//	gender: 'Female',
				//	birthdate: '24.04.2004',
				//	status: true,
				//	id: 1
				//}
			],
			studentsModals: {
				addVisibility: false,
				editVisibility: false,
				deleteVisibility: false,
				currentId: 0
			},
			taskLists: [
				//{
				//	name: 'ToDo',
				//	tasks: [
				//		{ name: 'Task #2', date: '2023-03-20', description: 'ff', id: 1 },
				//		{ name: 'Task #3', date: '2023-03-30', description: 'ff', id: 2 }
				//	],
				//	id: 1
				//},
				//{
				//	name: 'In Process',
				//	tasks: [
				//		{ name: 'Task #1', date: '2023-03-10', description: 'ff', id: 3 }
				//	],
				//	id: 2
				//},
				//{
				//	name: 'Done',
				//	tasks: [
				//		{ name: 'Task #0', date: '2023-03-01', description: 'ff', id: 4 }
				//	],
				//	id: 3
				//}
			]
		}
	}
})