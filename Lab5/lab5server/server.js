
const express = require('express');
const { access } = require('fs');
const app = express();
const http = require('http').Server(app);
const path = require('path');
//const bodyParser = require('body-parser');
const io = require('socket.io')(http);

const port = 3000;
const defaultProfileImageURL = 'https://www.svgrepo.com/show/497407/profile-circle.svg';

//database simulation
let users = [
    {
        login: 'hehe',
        password: 'nothehe',
        accessibleRooms: [1, 2],
        image: defaultProfileImageURL,
        name: 'hehe',
        socket: {},
        taskLists: [{
            name: 'ToDo',
            tasks: [
                { name: 'Task #2', date: '2023-03-20', description: 'ff', id: 1 },
                { name: 'Task #3', date: '2023-03-30', description: 'ff', id: 2 }
            ],
            id: 1
            },
		    {
            name: 'In Process',
            tasks: [
                { name: 'Task #1', date: '2023-03-10', description: 'ff', id: 3 }
            ],
            id: 2
            },
            {
            name: 'Done',
            tasks: [
                { name: 'Task #0', date: '2023-03-01', description: 'ff', id: 4 }
            ],
            id: 3
            }],
        notifications: [
            {
                room: {
                    name: 'Admin',
                    image: defaultProfileImageURL
                },
                text: 'test notification',
                id: (1 << 16) + 1
            }
        ],
        id: 1
    },
    {
        login: 'otherhehe',
        password: 'othernothehe',
        accessibleRooms: [1],
        image: defaultProfileImageURL,
        name: 'otherhehe',
        socket: {},
        taskLists: [],
        notifications: [],
        id: 2
    },
    {
        login: 'Admin',
        password: 'nothehe',
        accessibleRooms: [1, 2],
        image: defaultProfileImageURL,
        name: 'Admin',
        socket: {},
        taskLists: [],
        notifications: [],
        id: 3
    },
    {
        login: 'James Bond',
        password: 'nothehe',
        accessibleRooms: [1, 2],
        image: defaultProfileImageURL,
        name: 'James Bond',
        socket: {},
        taskLists: [],
        notifications: [],
        id: 4
    },
];
function getUser(id) {
    return users.find((user) => {
        return user.id === id;
    });
}

let rooms = [
    {
        name: 'Admin',
        membersId: [1, 2],
        messages: [
            { authorId: 3, text: 'blahblah', id: 1 },
            { authorId: 1, text: 'blahblahblah', id: 2 },
            { authorId: 4, text: 'blah?\nbruh', id: 3 }
        ],
        image: defaultProfileImageURL,
        id: 1
    },
    {
        name: 'Ann Smith',
        membersId: [1],
        messages: [],
        image: defaultProfileImageURL,
        id: 2
    }
];
function getRoom(id) {
    return rooms.find((room) => {
        return room.id === id;
    });
}

io.on('connection', (socket) => {
    socket.on('signIn', (authData) => {
        let userRecord = users.find((user) => {
            return user.login === authData.login && user.password === authData.password;
        });
        if (userRecord) {
            let accessibleRooms = rooms.filter((room) => {
                return userRecord.accessibleRooms.includes(room.id);
            }).map((room) => {
                return { name: room.name, id: room.id, image: room.image };
            });
            userRecord.socket = socket;
            socket.emit('successfulSignIn', {
                name: userRecord.name,
                rooms: accessibleRooms,
                image: userRecord.image,
                taskLists: userRecord.taskLists,
                notifications: userRecord.notifications,
                id: userRecord.id
            });
        }
        else {
            socket.emit('unsuccessfulSignIn');
        }
    });
    socket.on('signUp', (name, login, password) => {
        let userRecord = users.find((user) => {
            return user.login === login;
        });
        if (userRecord !== undefined) {
            socket.emit('unsuccessfulSignUp');
        } else {
            let nextId = users[users.length - 1].id + 1;
            users.push({
                login: login,
                password: password,
                accessibleRooms: [],
                image: defaultProfileImageURL,
                name: name,
                socket: socket,
                taskLists: [],
                id: nextId
            });
            socket.emit('successfulSignIn', {
                name: name,
                rooms: [],
                image: defaultProfileImageURL,
                taskLists: [],
                id: nextId
            });
        }
    });
    socket.on('fetchRoom', (userId, roomId, prevRoomId) => {
        let roomRecord = getRoom(roomId);
        if (!roomRecord.membersId.includes(userId)) {
            socket.emit('accessViolation', 'You don`t have access to this room');
        }
        let room = {
            name: roomRecord.name,
            members: roomRecord.membersId.map((memberId) => {
                let member = getUser(memberId);
                return {
                    name: member.name,
                    image: member.image,
                    id: memberId
                }
            }),
            messages: roomRecord.messages.map((message) => {
                let author = getUser(message.authorId);
                return {
                    author: {
                        name: author.name,
                        image: author.image,
                        id: message.authorId
                    },
                    text: message.text,
                    id: message.id
                };
            }),
            id: roomRecord.id
        };
        socket.emit('roomDataResponse', room);
        if (prevRoomId) {
            socket.leave(prevRoomId);
        }
        socket.join(room.id);
    });
    socket.on('sendMessage', (roomId, authorId, message) => {
        let roomRecord = getRoom(roomId);
        if (!roomRecord.membersId.includes(authorId)) {
            socket.emit('accessViolation', 'You don`t have access to this room');
        }
        let nextId = 1;
        if (roomRecord.messages.length !== 0) {
            nextId = roomRecord.messages[roomRecord.messages.length - 1].id + 1;
        }
        roomRecord.messages.push({
            authorId: authorId,
            text: message,
            id: nextId
        });
        let author = getUser(authorId);
        io.sockets.in(roomId).emit('newMessage', {
            author: {
                name: author.name,
                image: author.image,
                id: authorId
            },
            text: message,
            id: nextId
        });
        roomRecord.membersId.forEach((memberId) => {
            let member = getUser(memberId);
            let notification = {
                room: {
                    name: roomRecord.name,
                    image: roomRecord.image
                },
                text: message,
                id: (roomRecord.id << 16) + nextId
            };
            member.notifications.push(notification);
            if (Object.keys(member.socket).length !== 0) {
                member.socket.emit('notification', notification);
            }
        });
    });
    socket.on('notificationsReaded', (id) => {
        getUser(id).notifications.length = 0;
    });
    socket.on('createNewRoom', (memberId, roomName) => {
        let roomId = 1;
        if (rooms.length !== 0) {
            roomId = rooms[rooms.length - 1].id + 1;
        }
        let room = {
            name: roomName,
            membersId: [memberId],
            messages: [],
            image: defaultProfileImageURL,
            id: roomId
        }
        rooms.push(room);
        let userRecord = users.find((user) => {
            return user.id === memberId;
        });
        userRecord.accessibleRooms.push(room.id);
        socket.emit('newRoomAvailable', { name: room.name, image: room.image, id: room.id });
    });
    socket.on('changeProfile', (clientId, newName, newImage) => {
        let userRecord = getUser(clientId);
        userRecord.name = newName;
        userRecord.image = newImage;
        socket.emit('profileChanged');
    });
    socket.on('logOut', (clientId, roomId) => {
        let user = getUser(clientId);
        user.socket = {};
        socket.leave(roomId);
    });
    socket.on('searchMembers', (clientId, roomId, memberName) => {
        let roomRecord = getRoom(roomId);
        let availableMembers = users.filter((user) => {
            return user.name.toLowerCase().startsWith(memberName.toLowerCase())
                && !roomRecord.membersId.includes(user.id)
                && user.id !== clientId;
        }).map((user) => {
            return { name: user.name, image: user.image, id: user.id };
        });
        socket.emit('receiveMembers', availableMembers);
    });
    socket.on('addMember', (roomId, memberId) => {
        let roomRecord = getRoom(roomId);
        roomRecord.membersId.push(memberId);
        let userRecord = getUser(memberId);
        userRecord.accessibleRooms.push(roomId);
        if (Object.keys(userRecord.socket).length !== 0) {
            userRecord.socket.emit('newRoomAvailable', {
                name: roomRecord.name,
                image: roomRecord.image,
                id: roomRecord.id
            });
        }
        io.sockets.in(roomId).emit('newMemberConnected', {
            name: userRecord.name,
            image: userRecord.image,
            id: userRecord.id
        });
    });
    socket.on('leave', (roomId) => {
        socket.leave(roomId);
    });

    const students = [
        {
            group: 'KN-21',
            firstName: 'Ann',
            lastName: 'Bond',
            gender: 'Female',
            birthdate: '2004-04-24',
            status: true,
            id: 1
        },
        {
            group: 'KN-21',
            firstName: 'Ann',
            lastName: 'Bond',
            gender: 'Female',
            birthdate: '2004-04-24',
            status: true,
            id: 2
        }
    ];

    socket.on('fetchStudents', () => {
        socket.emit('studentsDataResponse', students);
    });
    socket.on('addStudent', (student) => {
        student.status = false;
        student.id = students[students.length - 1].id + 1;
        students.push(student);
        socket.emit('receiveAddedStudent', student);
    });
    socket.on('editStudent', (student) => {
        let studentToEdit = students.find((studentRecord) => {
            return studentRecord.id === student.id;
        });
        Object.assign(studentToEdit, student);
        socket.emit('receiveEditedStudent', studentToEdit);
    });
    socket.on('deleteStudent', (studentId) => {
        let studs = Object.assign([], students);
        students.length = 0;
        Object.assign(students, studs.filter((student) => student.id !== studentId));
        socket.emit('successfulDelete', studentId);
    });
    socket.on('changeStatus', (id) => {
        let student = students.find((student) => {
            return student.id === id;
        })
        student.status = !student.status;
        socket.emit('statusChanged', id);
    });

    function addTask(userId, listId, board, name, date, description) {
        let user = getUser(userId);
        let taskList = user.taskLists.find((list) => {
            return list.name === board;
        });
        let nextId = 1;
        if (taskList.tasks.length !== 0) {
            nextId = taskList.tasks[taskList.tasks.length - 1].id + 1;
        }
        taskList.tasks.push({
            name: name,
            date: date,
            description: description,
            id: nextId
        });
        socket.emit('newTask', taskList.id, taskList.tasks[taskList.tasks.length - 1]);
    }
    socket.on('addTask', (userId, listId, board, name, date, description) => {
        addTask(userId, listId, board, name, date, description);
    });
    socket.on('editTask', (userId, listId, board, taskId, name, date, description) => {
        let user = getUser(userId);
        let taskList = user.taskLists.find((list) => {
            return list.id === listId;
        });
        let taskListNew = user.taskLists.find((list) => {
            return list.name === board;
        });
        if (taskList === taskListNew) {
            let task = taskList.tasks.find((task) => {
                return task.id === taskId;
            });
            let newTask = {
                name: name,
                date: date,
                description: description,
                id: taskId
            };
            Object.assign(task, newTask);
            newTask.listId = taskList.id;
            socket.emit('editedTask', newTask);
        } else {
            let taskListCopy = Object.assign([], taskList.tasks);
            taskList.tasks.length = 0;
            Object.assign(taskList.tasks, taskListCopy.filter((task) => task.id !== taskId));
            socket.emit('deleteTask', taskId, listId);
            addTask(userId, taskListNew.id, board, name, date, description);
        }
    });
});

//app.use(bodyParser.json());
app.use(express.static('../lab5client/dist'));

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '../lab5client/dist/index.html'));
});

app.get('/messages/', (req, res) => {
    res.sendFile(path.join(__dirname, '../lab5client/dist/index.html'));
});

app.get('/students/', (req, res) => {
    res.sendFile(path.join(__dirname, '../lab5client/dist/index.html'));
});

app.get('/tasks/', (req, res) => {
    res.sendFile(path.join(__dirname, '../lab5client/dist/index.html'));
});

http.listen(port);