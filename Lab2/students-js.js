
function notification() {
    const anim = [
        { opacity: 0 },
        { opacity: 1 },
    ];

    const params = {
        duration: 1000,
        delay: 1000,
        iterations: 5,
        direction: "alternate",
        fill: "forwards",
    };

    document.getElementById("notification-circle").animate(anim, params);
}

function openNotifications() {
    document.getElementById("notification-window").style.display = "block";
}

function closeNotifications() {
    document.getElementById("notification-window").style.display = "none";
}

function openProfile() {
    document.getElementById("profile-window").style.display = "block";
}

function closeProfile() {
    document.getElementById("profile-window").style.display = "none";
}

function openAddStudentModal() {
    $("form[name=add-edt-form]").trigger("reset");
    $("#add-edt-modal-heading").html("Add student");
    $("#add-edt-modal-ok-btn").attr("onclick", "addStudent()"); //
    $("#add-edt-modal").show();
}

function closeAddStudentModal() {
    $("#add-edt-modal").hide();
}

let rowToEdit;

function openEditStudentModal(event) {
    rowToEdit = event.target.parentNode.parentNode.parentNode.parentNode;
    let data = getRowData(rowToEdit);
    setAddEdtFormData(data);
    $("#add-edt-modal-heading").html("Edit student");
    $("#add-edt-modal-ok-btn").attr("onclick", "editStudent()"); //
    $("#add-edt-modal").show();
}

function closeEditStudentModal() {
    $("#add-edt-modal").hide();
}

function formString() {
    alert($("form[name=add-edt-form]").serialize());
}

function getAddEdtFormData() {
    let group = $("form[name=add-edt-form] [name=group]").val();
    let firstName = $("form[name=add-edt-form] [name=first-name]").val();
    let lastName = $("form[name=add-edt-form] [name=last-name]").val();
    let gender = $("form[name=add-edt-form] [name=gender]").val();
    let bdStr = $("form[name=add-edt-form] [name=birthdate]").val();
    let birthdate = new Date(bdStr);
    return [group, firstName, lastName, gender, birthdate];
}

function setAddEdtFormData([group, firstName, lastName, gender, birthdate]) {
    $("form[name=add-edt-form] [name=group]").val(group);
    $("form[name=add-edt-form] [name=first-name]").val(firstName);
    $("form[name=add-edt-form] [name=last-name]").val(lastName);
    $("form[name=add-edt-form] [name=gender]").val(gender);
    let bdStr = birthdate.getFullYear() + "-" + String(birthdate.getMonth() + 1).padStart(2, "0") + "-" + String(birthdate.getDate()).padStart(2, "0");
    $("form[name=add-edt-form] [name=birthdate]").val(bdStr);
}

function validateAndGetAddEdtForm() {
    let [group, firstName, lastName, gender, birthdate] = getAddEdtFormData();

    let letters = /^[A-Za-z]+$/;
    let maxDate = new Date(2023, 1, 1);
    let minDate = new Date(1923, 1, 1);

    //i don`t care about it due to "required"
    if (firstName == "" || lastName == "" || birthdate == "") {
        return false;
    }

    if (!firstName.match(letters)) {
        alert("Uncorrect first name");
        $("form[name=add-edt-form] [name=first-name]").select();
        event.preventDefault();
        return false;
    } else if (!lastName.match(letters)) {
        alert("Uncorrect last name");
        $("form[name=add-edt-form] [name=last-name]").select();
        event.preventDefault();
        return false;
    } else if (birthdate > maxDate || birthdate < minDate) {
        alert("Uncorrect birthdate");
        $("form[name=add-edt-form] [name=birthdate]").select();
        event.preventDefault();
        return false;
    }
    return [group, firstName, lastName, gender, birthdate];
}

function createRow() {
    $("#main-table tbody").append(
        '<tr>\
            <td>\
                <input type="checkbox" onclick="changeState(event)" />\
            </td>\
            <td></td>\
            <td></td>\
            <td></td>\
            <td></td>\
            <td>\
                <div class="status"></div>\
            </td>\
            <td>\
                <button onclick="openEditStudentModal(event)">\
                    <div class="btn-icon">\
                        <img src="./img/edit.svg" alt="E" height="28px" />\
                    </div>\
                </button>\
                <button onclick="openWarningModal(event)">\
                    <div class="btn-icon">\
                        <img src="./img/delete.svg" alt="D" height="28px" />\
                    </div>\
                </button>\
            </td>\
        </tr>'
    );
}

function fillRow(row, data) {
    $(row).children().eq(1).html(data[0]);
    $(row).children().eq(2).html(data[1] + " " + data[2]);
    $(row).children().eq(3).html(data[3][0]);
    $(row).children().eq(4).html(String(data[4].getDate()).padStart(2, "0") + "." + String(data[4].getMonth() + 1).padStart(2, "0") + "." + data[4].getFullYear());
}

function getRowData(row) {
    let group = $(row).children().eq(1).text();
    let firstName = $(row).children().eq(2).text().split(" ")[0];
    let lastName = $(row).children().eq(2).text().split(" ")[1];
    let gender = $(row).children().eq(3).text() == "M" ? "Male" : "Female";
    let bdStr = $(row).children().eq(4).text().split(".");
    let birthdate = new Date(parseInt(bdStr[2], 10),
                             parseInt(bdStr[1], 10) - 1,
                             parseInt(bdStr[0], 10));
    return [group, firstName, lastName, gender, birthdate];
}

function addStudent() {
    let validationResult = validateAndGetAddEdtForm();
    if (validationResult === false) return false;
    createRow();
    fillRow(document.getElementById("main-table").children[0].lastChild, validationResult);
    closeAddStudentModal();
    formString();
    event.preventDefault();
}

function editStudent() {
    let validationResult = validateAndGetAddEdtForm();
    if (validationResult === false) return false;
    fillRow(rowToEdit, validationResult);
    closeEditStudentModal();
    formString();
    event.preventDefault();
}

function changeState(event) {
    let checkbox = event.target;
    let row = checkbox.parentNode.parentNode;
    let statusDiv = row.children[5].children[0];
    if (checkbox.checked) statusDiv.style.backgroundColor = "greenyellow";
    else statusDiv.style.backgroundColor = "lightgrey";
}

let rowToRemove;

function openWarningModal(event) {
    let img = event.target;
    rowToRemove = img.parentNode.parentNode.parentNode.parentNode;
    let name = rowToRemove.children[2].innerText;
    document.getElementById("del-name").innerText = name;
    document.getElementById("del-modal").style.display = "block";
}

function closeWarningModal() {
    document.getElementById("del-modal").style.display = "none";
}

function deleteRow() {
    rowToRemove.remove();
    closeWarningModal();
}